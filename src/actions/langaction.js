import { UPDATE_LANG } from './types'

export function langUpdate(newLang) {
  return function(dispatch) {
    dispatch({
      type: UPDATE_LANG,
      lang : newLang
    }) 
  }
}