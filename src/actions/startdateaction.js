import { UPDATE_START_DATE } from './types'

export function startDateUpdate(newDate) {
  return function(dispatch) {
    dispatch({
      type: UPDATE_START_DATE,
      startDate : newDate
    }) 
  }
}