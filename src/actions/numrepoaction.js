import { UPDATE_NUM_REPOS } from './types'

export function numRepoUpdate(newNum) {
  return function(dispatch) {
    dispatch({
      type: UPDATE_NUM_REPOS,
      numRepo : newNum
    }) 
  }
}