// TODO: Known Issue: if requested number of repos is to high nothing is rendered

import { FETCH_REPOS } from './types'

export const fetchRepos = (lang, numRepos, date) => dispatch => {
  try {
    fetch('https://api.github.com/search/repositories?q=language:' + lang + '+created:>' + date + '&sort=stars&order=desc')
    .then(response => response.json())
    .then(responseJson => {
      let tempTitles = [];
      let tempStar = [];
      let tempLink = [];
      let tempDate = [];
        for (let i = 0; i < numRepos; i++) {        
          let object = responseJson.items[i]            
          tempTitles.push(object.name)
          tempStar.push(object.stargazers_count)
          tempLink.push(object.html_url)
          tempDate.push((object.created_at.slice(0, 10)))
        }
          dispatch({
            type: FETCH_REPOS,
            titles : tempTitles,
            stars : tempStar,
            links : tempLink,
            dates : tempDate,
          })
        })
      } 
      catch(error){
        console.error(error)
      }
    }