import { combineReducers } from 'redux'
import numRepoReducer from './numreporeducer'
import langReducer from './langreducer'
import startDateReducer from './startdatereducer'
import fetchRepos from './fetchreposreducer'

export default combineReducers({
  numRepo : numRepoReducer,
  lang : langReducer,
  startDate : startDateReducer,
  repos : fetchRepos,
})