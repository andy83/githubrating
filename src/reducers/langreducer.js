import { UPDATE_LANG } from '../actions/types'

const initalState = {
  lang : '',
}

export default function(state = initalState, action) {
  
  switch(action.type) {
    case UPDATE_LANG:
    return {
      lang : action.lang
    }
    default:
      return state
  }
}