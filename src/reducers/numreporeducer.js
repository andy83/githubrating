import { UPDATE_NUM_REPOS } from '../actions/types'

const initalState = {
  numRepo : '',
}

export default function(state = initalState, action) {
  
  switch(action.type) {
    case UPDATE_NUM_REPOS:
    return {
      numRepo : action.numRepo
    }
    default:
      return state
  }
}