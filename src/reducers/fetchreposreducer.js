import { FETCH_REPOS } from '../actions/types'

const initalState = {
  titles : [],
  stars : [],
  links : [],
  dates : [],
}

export default function(state = initalState, action) {
  
  switch(action.type) {    
    case FETCH_REPOS:
    return {      
      titles : action.titles,
      stars : action.stars,
      links : action.links,
      dates : action.dates
    }
    default:
      return state
  }
}