import { UPDATE_START_DATE } from '../actions/types'

const initalState = {
  startDate : '',
}

export default function(state = initalState, action) {
  
  switch(action.type) {
    case UPDATE_START_DATE:
    return {
      startDate : action.startDate
    }
    default:
      return state
  }
}