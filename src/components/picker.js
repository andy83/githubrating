import React, { Component } from 'react'

class Picker extends Component {
  
  constructor() {
    super()
    this.handleSelection = this.handleSelection.bind(this)
  }

  handleSelection (event) {    
    this.props.onChange(event.target.value)
  }
  
  render() {
    return (
      <div className='Picker' style={styles.main}>
        <select 
          value={this.props.selectedValue} 
          onChange={this.handleSelection} 
          style={this.props.style}>
          {this.props.labels.map((label) => {
            return <option key={label} value={label}>{label}</option>
          })}
        </select>     
      </div>
    )
  }
}

export default Picker;

const styles = {
  main: {

  }
}
