import React, { Component } from 'react';

class ListItem extends Component {

  render() {
    return (
      <div className='ListItem' style={styles.main} >
          <li>
            <div className='Wrapper' style={styles.wrapper}>
              <div className='DataItem' style={styles.dataItem}>{this.props.titles}</div>
              <div className='DataItem' style={styles.dataItem}>Star Ranking : {this.props.starRank}</div> 
            </div>
            <div className='Wrapper' style={styles.wrapper}>
              <div className='DataItem' style={styles.dataItem}><a href={this.props.repoLink}>Link</a></div> 
              <div className='DataItem' style={styles.dataItem}>creation date : {this.props.creationDate}</div>
            </div>
          </li>
      </div>
    );
  }
}

export default ListItem;

const styles = {
  main: {
    listStyleType : 'none',
    borderStyle : 'solid',
    borderWidth : 1,
    width : 'auto',
    marginBottom : 3,
    padding : 10, 
  },
  wrapper: {
    display : 'flex',
  },
  dataItem: {
    margin : 5,
    padding : 5
  },
};