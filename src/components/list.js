import React, { Component } from 'react';
import {ListItem} from '.';

class List extends Component {

  render() {
    return (
      <div className='List' style={styles.main} >
        <ul>{
            this.props.titles.map(function(title, i){
              return <ListItem 
                        key={i} 
                        titles={title}
                        starRank={this.props.starRank[i]}
                        repoLink={this.props.repoLink[i]}
                        creationDate={this.props.creationDate[i]}
                      />
            }, this)
          }
        </ul>       
      </div>
    );
  }
}

export default List;

const styles = {
  main: {

  }
};