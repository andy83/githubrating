import React, { Component } from 'react';

class TextInput extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange (event) {   
    this.props.onChange(event.target.value)
  }

  render() {
    return (
      <div className='TextInput' style={styles.main} >
        <input 
          type='text'
          value={this.props.number}        
          onChange={this.handleChange}
          style={this.props.style}
        />
      </div>
    );
  }
}

export default TextInput;

const styles = {
  main: {

  }
};