import React, { Component } from 'react';

class SearchButton extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick (event) {   
    this.props.onClick(event.target.value)
  }

  render() {
    return (
      <div className='SearchButton' style={styles.main} >
        <button onClick={this.handleClick} style={this.props.style}>Search</button>
      </div>
    );
  }
}

export default SearchButton;

const styles = {
  main: {

  }
};