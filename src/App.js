import React, { Component } from 'react';
import { Provider } from 'react-redux';

import {Header, Footer, Body} from './layout';
import store from './store';



class App extends Component {
  render() {    
    return (
      <Provider store={store}>
        <div className='App'>
          <Header />
          <Body />
          <Footer />
        </div>
      </Provider>
      
    );
  }
}

export default App;
