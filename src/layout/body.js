// TODO: Do something pretty with returned result, grab repo icons

import React, { Component } from 'react';
import { List } from '../components';
import { connect } from 'react-redux';

class Body extends Component {

 render() {
    return (
      <div className='Body' style={styles.main} >
        <List 
          titles={this.props.titles}
          starRank={this.props.stars}
          repoLink={this.props.links}
          creationDate={this.props.dates}
        />        
      </div>
    );
  }
}

const mapStateToProps = state => ({
  titles : state.repos.titles,
  stars : state.repos.stars,
  links : state.repos.links,
  dates : state.repos.dates,
})

export default connect(mapStateToProps)(Body);

const styles = {
  main: {
    display : 'flex',
    justifyContent : 'center',
    marginTop : 200
  }
};