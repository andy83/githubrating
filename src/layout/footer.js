// TODO: Make footer sticky on scroll

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { numRepoUpdate } from '../actions/numrepoaction'
import { langUpdate } from '../actions/langaction'
import { startDateUpdate } from '../actions/startdateaction'
import { fetchRepos } from '../actions/fetchreposaction'
import { Picker, SearchButton, TextInput } from '../components';
import { Paper } from '@material-ui/core';


class Footer extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      languages : [],
      months : ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      years : ['', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019'],
      selectedLang: '',
      numRepos : '',
      selectedMonth: '',
      selectedYear: '',
    };
    this.numberChange = this.numberChange.bind(this)
    this.languageChange = this.languageChange.bind(this)
    this.monthChange = this.monthChange.bind(this)
    this.yearChange = this.yearChange.bind(this)
    this.searchGit = this.searchGit.bind(this)
  }

  // Fetch Languages for select element
  // Choose from every language on GitHub

  // componentDidMount() {  
  //   try {
  //     const templang = [''];  
  //     fetch('https://github-trending-api.now.sh/languages')
  //     .then(responce => responce.json())
  //     .then(responseJson => { 
  //     for (let i in responseJson.all) {
  //       let object = responseJson.all[i]        
  //       templang.push(object.name)
  //     }   
  //     this.setState({languages : templang})    
  //   }) 
  // }
  // catch(error) {
  //   console.error(error)
  // }    

  // Fetch Languages for select element
  // Choose from popular languages on GitHub

  componentDidMount() {
    try {
        const templang = [''];  
        fetch('https://github-trending-api.now.sh/languages')
        .then(responce => responce.json())
        .then(responseJson => { 
        for (let i in responseJson.popular) {
          let object = responseJson.popular[i]        
          templang.push(object.name)
        }   
        this.setState({languages : templang})    
      }) 
    }
    catch(error) {
      console.error(error)
    }    
  }

  languageChange(lang) {
    this.setState({ selectedLang : lang })
  }

  numberChange(number) {    
    this.setState({ numRepos : number })    
  }

  monthChange(month) {
    this.setState({ selectedMonth : month })
  }

  yearChange(year) {
    this.setState({ selectedYear : year })
  }

  searchGit() {
    let parsedNum = parseInt(this.state.numRepos)
    if ( this.state.selectedLang === '' 
      || this.state.selectedMonth === '' 
      || this.state.selectedYear === '' 
      || this.state.numRepos === '') 
      {
      alert('Please finish making your selections')
      }
    else {
      if (Number.isInteger(parsedNum) === false) {
        alert('Please input an appropriate number e.g 10')
      }
      else{
        if ( Number.isInteger(parsedNum) ) {
          let monthNum = ('0' + (this.state.months.indexOf(this.state.selectedMonth))).slice(-2)
          let searchDate = this.state.selectedYear + '-' + monthNum  + '-01'
          let startDate = this.state.selectedMonth + ' ' + this.state.selectedYear
          this.props.startDateUpdate(startDate)
          this.props.numRepoUpdate(this.state.numRepos)
          this.props.langUpdate(this.state.selectedLang)
          this.props.fetchRepos(this.state.selectedLang, this.state.numRepos, searchDate)
        } 
      }
    }
  }
  
  
  render() {
    return (
      <div className='Footer' style={styles.main} >
        <Paper style={styles.paper}>
          <Picker 
            labels={this.state.languages}
            selectedValue={this.state.selectedLang}
            onChange={this.languageChange}
            style={styles.contEle}
          />
          <TextInput
            number={this.state.numRepos}
            onChange={this.numberChange}
            style={styles.contEle}
          />
          <Picker
            labels={this.state.months}
            onChange={this.monthChange}
            style={styles.contEle}
          />
          <Picker
            labels={this.state.years}
            onChange={this.yearChange}
            style={styles.contEle}
          />
          <SearchButton onClick={this.searchGit} style={styles.contEle}/>  
        </Paper>
      </div>
    );
  }
}


export default connect(null, {numRepoUpdate, langUpdate, startDateUpdate, fetchRepos})(Footer);

const styles = {
  main: {

  },
  paper : {
    display: 'flex',
    justifyContent: 'center',
    padding: 50
  },
  contEle : {
    margin: 15
  }

};