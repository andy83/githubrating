import Header from './header';
import Footer from './footer';
import Body from './body';

export {
  Header, Footer, Body
}