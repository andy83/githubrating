// TODO: on mount have a message giving instructions

import React, { Component } from 'react';
import { AppBar, Typography } from '@material-ui/core';
import { connect } from 'react-redux';


class Header extends Component {

  render() {
    return (      
      <div className='Header' style={styles.main} > 
        <AppBar postion='static'>        
            <div className='TopText' style={styles.topText}>
              <Typography variant='h2'>
                  Top {this.props.numRepo.numRepo} Ranked Repos According To Stars Written In {this.props.lang.lang}
              </Typography>
            </div>
            <div className='BottomText' style={styles.bottomText}>
              <Typography variant='h4'>
                Repo's created since {this.props.startDate.startDate}
              </Typography>
            </div>       
        </AppBar> 
      </div>
    );
  }
}

const mapStateToProps = state => ({
  numRepo: state.numRepo,
  lang : state.lang,
  startDate : state.startDate
})

export default connect(mapStateToProps)(Header);

const styles = {
  main: {

  },
  paper: {
    
  },
  topText: {
    padding : 15,
    display : 'flex',
    justifyContent : 'center'
  },
  bottomText: {
    display : 'flex',
    justifyContent : 'center',
    padding : 15,
  },
};